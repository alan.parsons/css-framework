<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Menu</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/menu.less')); ?>

        <h2 class="e-preview-title js-preview">Menu</h2>
        <div class="js-markup" brush="xml">
            <ul class="mf e-menu">
                <li class="is-current s-item">
                    <a href="" class="s-link">Home</a>
                </li>
                <li class="s-item mf e-dropdown">
                    <a href="" class="s-link has-icon">About <i class="e-icon has-dropdown fa fa-caret-down"></i></a>
                    <ul class="e-menu">
                        <li class="s-item">
                            <a href="" class="s-link">Item 1</a>
                        </li>
                        <li class="s-item">
                            <a href="" class="s-link">Item 2</a>
                        </li>
                    </ul>
                </li>
                <li class="s-item">
                    <span class="s-link">This is just text</span>
                </li>
            </ul>
        </div>

        <h2 class="e-preview-title js-preview">Vertical</h2>
        <div class="js-markup" brush="xml">
            <ul class="mf e-menu m-vertical">
                <li class="is-current s-item">
                    <a href="" class="s-link">Home</a>
                </li>
                <li class="s-item mf e-dropdown m-vertical">
                    <a href="" class="s-link has-icon">About <i class="e-icon has-dropdown fa fa-caret-right"></i></a>
                    <ul class="e-menu">
                        <li class="s-item">
                            <a href="" class="s-link">Item 1</a>
                        </li>
                        <li class="s-item">
                            <a href="" class="s-link">Item 2</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <h2 class="e-preview-title js-preview">Tabbed</h2>
        <div class="js-markup" brush="xml">
            <ul class="mf e-menu m-tabular">
                <li class="is-current s-item">
                    <a href="" class="s-link">Home</a>
                </li>
                <li class="s-item mf e-dropdown">
                    <a href="" class="s-link has-icon">About <i class="e-icon has-dropdown fa fa-caret-down"></i></a>
                    <ul class="e-menu">
                        <li class="s-item">
                            <a href="" class="s-link">Item 1</a>
                        </li>
                        <li class="s-item">
                            <a href="" class="s-link">Item 2</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <!-- END .main -->

<?php include('partials/footer.php') ?>
