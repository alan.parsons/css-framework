<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">

        <h1 class="h1">Cycle with Velocity</h1>
        <hr/>
        <p>Creates ability to animate sections of text within a banner.</p>
        <p>Animations are applied via the data-animatein attr example below:</p>
        <div class="e-markup m-good">
            <pre class="brush: xml">
                <div data-animatein="opacity:1;translateZ:0;translateX:200px;rotateX:360deg;duration:2000;easing:250,15">...</div>
            </pre>
        </div>
        <p>You will need to apply CSS styles specifically for each animated item so that it starts and ends in the correct places. Dont forget in IE8 the end position is the start position.</p>
        <div class="js-cycle cycle" data-cycle-slides="> .slide">
            <div class="slide">
                <span class="slide-content">
                    <span class="inset">
                        <span class="slide-animate" data-animatein="opacity:1;translateZ:0;translateX:200px;rotateX:360deg;duration:2000;easing:250,15">
                            <span>Text</span>
                        </span>
                    </span>
                </span>
                <img src="http://placehold.it/1540x440">
            </div>
            <div class="slide">
                <span class="slide-content">
                    <span class="inset">
                        <span class="slide-animate" data-animatein="opacity:1;translateZ:100px,250,15;translateX:200px;rotateX:360deg;duration:2000">
                            <span>Text</span>
                        </span>
                    </span>
                </span>
                <img src="http://placehold.it/1540x440">
            </div>
            <div class="slide">
                <span class="slide-content">
                    <span class="inset">
                        <span class="slide-animate" data-animatein="opacity:1;translateZ:0;translateX:200px;rotateZ:-45deg;duration:1000">
                            <span>Text</span>
                        </span>
                    </span>
                </span>
                <img src="http://placehold.it/1540x440">
            </div>
            <div class="slide">
                <span class="slide-content">
                    <span class="inset">
                        <span class="slide-animate" data-animatein="opacity:1;translateZ:0;translateX:100px;rotateZ:360deg;duration:2000;easing:250,15">
                            <span>Text</span>
                        </span>
                    </span>
                </span>
                <img src="http://placehold.it/1540x440">
            </div>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
