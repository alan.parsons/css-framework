<?php include('partials/header.php') ?>

    <div class="l-container">
        <h1 class="h1">Grid</h1>
        <hr/>

        <?php fileInfo(array('location' => 'layout/grid.less')); ?>

        <h2 class="e-preview-title js-preview">Demo</h2>
        <div class="js-markup" brush="xml">
            <div class="l-grid">
                <div class="l-column l-lg-4">
                    <p>This l-grid system is made of 12 but you can adjust as needed.</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                </div>
                <div class="l-column l-lg-4">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                </div>
                <div class="l-column l-lg-4">
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                </div>
            </div>
        </div>

    </div>
    <!-- END .main -->

<?php include('partials/footer.php') ?>
