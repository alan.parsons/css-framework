<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Button</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/button.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <a href="" title="" class="mf e-button">Button</a>
        </div>

        <h2 class="e-preview-title js-preview">Colours</h2>
        <div class="js-markup" brush="xml">
            <a href="" title="" class="mf e-button m-blue">Blue</a>
            <a href="" title="" class="mf e-button m-orange">Orange</a>
            <a href="" title="" class="mf e-button m-yellow">Yellow</a>
            <a href="" title="" class="mf e-button m-purple">Purple</a>
            <a href="" title="" class="mf e-button m-positive">Positive</a>
            <a href="" title="" class="mf e-button m-negative">Negative</a>
            <a href="" title="" class="mf e-button m-inverted">Inverted</a>
            <a href="" title="" class="mf e-button m-basic">Basic</a>
            <a href="" title="" class="mf e-button m-blue m-basic">Basic Blue</a>
            <a href="" title="" class="mf e-button m-link">Link</a>
            <a href="" title="" class="mf e-button m-blue m-link">Link Blue</a>
        </div>

        <h2 class="e-preview-title js-preview">States</h2>
        <div class="js-markup" brush="xml">
            <a href="" title="" class="mf e-button is-disabled">Button</a>
            <a href="" title="" class="mf e-button is-loading">Loading</a>
            <a href="" title="" class="mf e-button is-loading m-inverted">Loading</a>
            <a href="" title="" class="mf e-button has-icon m-circular"><i class="e-icon fa fa-diamond"></i></a>
        </div>

        <h2 class="e-preview-title js-preview">Buttons</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-buttons u-block">
                <a href="" title="" class="mf e-button m-orange">Orange</a>
                <a href="" title="" class="mf e-button m-yellow">Yellow</a>
                <a href="" title="" class="mf e-button m-purple">Purple</a>
            </div>
        </div>
    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
