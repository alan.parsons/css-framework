<?php include('partials/header.php') ?>

    <!-- START .main -->

    <div class="l-container">
        <h1 class="h1">Overview</h1>
        <hr/>
        <div class="l-grid">
            <div class="l-column l-lg-12">
                <p>This framework is not here to build the site for you but is to give you the best possible start flattening out browser issues and making everything on a level field giving you the building blocks.</p>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">CSS</h1>
                <hr/>
                <p>Before starting a build make sure you read through the best practices for coding CSS <a href="guidelines.php">CSS Guidelines</a></p>
                <p>CSS3 prefixes are called using prefixfree.min.js so we have a smaller CSS file and this will inlcude the prefixes only when needed</p>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">Hooks &amp; Vars</h1>
                <hr/>
                <p>This framework uses hooks, variables to be able to ammend the less files and these should be used when needed. When building your site you should also remember to add the correct variables and mixins to allow ease of use for someone else and to amend in the future</p>
                <p>An example of this would be the <code>@vars__ie8-support</code> var this is used to add needed CSS to make IE8 behave but once we done with IE8 we dont want all that CSS hanging around for the ride instead with the <code>& when</code> statment we can exlude all that CSS from being outputed by changing this var to false.</p>
                <p>Default Variables: <code class="js-location">variables.less</code></p>
                <div class="js-variables"></div>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">Icons</h1>
                <hr/>
                <p>The default icon set used by this framework is <a href="http://fortawesome.github.io/Font-Awesome/icons/">Font Awesome</a></p>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">No Header/Footer</h1>
                <hr/>
                <p>There is no default header and footer as these can change on a site per site basis, templates may be included at a later stage if enough are used again and again.</p>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">Javascript</h1>
                <hr/>
                <p>If you are writing a plugin please use the boilerplate one provided in the SVN.</p>
                <p>The marjority of jquery files are pulled in using one file and require.js this is to increase load speed calling the js asynchronously see <a href="http://requirejs.org/">http://requirejs.org/</a>.</p>
            </div>
            <div class="l-column l-lg-12">
                <h1 class="h2">To Do</h1>
                <hr/>
                <p>A list of things that need to be added to the framework.</p>
                <ul>
                    <li>Accordion</li>
                    <li>Expand to have size classes</li>
                    <li>Typo variables need adding</li>
                    <li>Menu toggle @tablet ??</li>
                    <li>Break "a" tags @mobile ??</li>
                    <li>Schema tags</li>
                    <li>Screen readers</li>
                    <li>Accessibility complient?</li>
                    <li>Expand sections details how to use etc.</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
