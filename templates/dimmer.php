<?php include('partials/header.php') ?>

    <!-- START .main -->

    <div class="l-container">
        <h1 class="h1">Dimmer</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/dimmer.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml" style="position: relative; min-height: 200px;">
            <div class="mf e-dimmer is-visible"></div>
        </div>

        <h2 class="e-preview-title js-preview">Text</h2>
        <div class="js-markup" brush="xml" style="position: relative; min-height: 200px;">
            <div class="mf e-dimmer is-visible"><div class="s-text">Loading...</div></div>
        </div>

        <h2 class="e-preview-title js-preview">Loader</h2>
        <div class="js-markup" brush="xml" style="position: relative; min-height: 200px;">
            <div class="mf e-dimmer is-visible has-loader"><div class="e-loader s-text">Loading...</div></div>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
