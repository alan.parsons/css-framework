<?php include('functions.php') ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Frontend Framework</title>

    <!-- GENERATE favicons here http://realfavicongenerator.net/ -->

    <link rel="icon" type="image/x-icon" href="images/favicon/favicon.ico" />
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/favicon/apple-touch-icon-114x114-precomposed.png">
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/favicon/apple-touch-icon-72x72-precomposed.png">
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-57x57-precomposed.png">

    <!-- Framework -->
    <link href="../assets/css/core.css" rel="stylesheet" />
    <!-- Plugins -->

    <script src="../assets/js/vendor/modernizr-2.6.2.min.js"></script>

    <!-- START IE8 Scripts -->
    <!-- Loaded here as we need them straight away to make sure the page doesnt look poop -->
	<!--[if IE 8]>
    <script src="../assets/js/vendor/ie/respond.min.js"></script>
    <script src="../assets/js/vendor/ie/tokenizer.js"></script>
    <script src="../assets/js/vendor/ie/parser.js"></script>
    <script src="../assets/js/vendor/ie/rem.min.js"></script>
	<![endif]-->
    <!-- END IE8 Scripts -->

    <!-- START DEMO ONLY -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/styles/shCore.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/styles/shThemeDefault.min.css" rel="stylesheet" />
    <link href="../demo/demo.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/scripts/shCore.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/scripts/shBrushCss.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/scripts/shBrushJScript.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/SyntaxHighlighter/3.0.83/scripts/shBrushXml.min.js"></script>
    <script>
        SyntaxHighlighter.defaults['gutter'] = false;
        SyntaxHighlighter.defaults['smart-tabs'] = false;
        SyntaxHighlighter.all();
    </script>
    <!-- END DEMO ONLY -->

</head>
<body>

    <?php
    $menu = array(
        array(
            'label' => 'Overview',
            'url' => 'index.php'
        ),
        array(
            'label' => 'Guidelines',
            'url' => 'guidelines.php'
        ),
        array(
            'label' => 'Cycle/Velocity Banner',
            'url' => 'banner.php'
        ),
        array(
            'label' => 'Typo',
            'url' => 'typo.php'
        ),
        array(
            'label' => 'Field',
            'url' => 'field.php'
        ),
        array(
            'label' => 'Form',
            'url' => 'form.php'
        ),
        array(
            'label' => 'Sidebar',
            'url' => 'sidebar.php'
        ),
        array(
            'label' => 'Modal',
            'url' => 'modal.php'
        ),
        array(
            'label' => 'Tab',
            'url' => 'tab.php'
        ),
        array(
            'label' => 'Accordion',
            'url' => 'accordion.php'
        ),
        array(
            'label' => 'Button',
            'url' => 'button.php'
        ),
        array(
            'label' => 'Menu',
            'url' => 'menu.php'
        ),
        array(
            'label' => 'Badge',
            'url' => 'badge.php'
        ),
        array(
            'label' => 'Section',
            'url' => 'section.php'
        ),
        array(
            'label' => 'Message',
            'url' => 'message.php'
        ),
        array(
            'label' => 'Grid',
            'url' => 'grid.php'
        ),
        array(
            'label' => 'Table',
            'url' => 'table.php'
        ),
        array(
            'label' => 'Loader',
            'url' => 'loader.php'
        ),
        array(
            'label' => 'Dimmer',
            'url' => 'dimmer.php'
        ),
    );
    ?>

    <header class="primary l-header">
        <div class="l-container">
            <a href="#sidebar-menu" data-sidebar-history="false" class="mf e-button m-blue js-sidebar">Menu</a>
        </div>
    </header>

    <!-- Modal -->
    <div class="mf e-sidebar" id="sidebar-menu" aria-hidden="true">
        <a href="#close" class="s-overlay" aria-hidden="true"></a>
        <div class="s-content">
            <div class="s-header">
                <a href="#close" class="s-close" aria-hidden="true">×</a> <!--CHANGED TO "#close"-->
                Menu
            </div>
            <div class="s-body">
                <ul class="mf e-menu m-vertical">
                    <?php
                        $url  = $_SERVER["PHP_SELF"];
                        $path = explode("/", $url);
                        $last = end($path);
                        for ($x = 0; $x < count($menu); $x++) {
                            if (strpos($last, $menu[$x]['url']) !== false) {
                                echo '<li class="s-item is-current">';
                            } else {
                                echo '<li class="s-item">';
                            }
                            echo '<a href="' . $menu[$x]['url'] . '" class="s-link">' . $menu[$x]['label'] . '</a>';
                            echo '</li>';
                        }
                    ?>
                </ul>
            </div>
            <div class="s-footer">

            </div>
        </div>
    </div>
    <!-- /Modal -->