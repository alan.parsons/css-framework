
<?php

function fileInfo($args) {

    extract($args);

    $output = '';
    $output .= '<div class="u-clearfix e-section">';
    $output .= '<div class="js-buttons e-buttons u-float-right"></div>';
    $output .= '<div class="e-buttons u-float-right">';
    $output .= '<div class="e-button m-link">Location: <code class="js-location">'.$location.'</code></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="js-variables"></div>';
    $output .= '<div class="js-hooks"></div>';

    echo $output;

}

?>