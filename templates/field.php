<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Field</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/field.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field">
                <label class="s-label">Text Input</label>
                <div class="mf s-control">
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Disabled</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field">
                <label class="s-label">Disabled Input</label>
                <input class="s-input" disabled type="text"  placeholder="Text Input" />
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Negative</h2>
        <div class="js-markup" brush="xml">
            <div class="mf is-negative e-field">
                <label class="s-label">Negative Input</label>
                <div class="mf s-control">
                    <div class="mf e-badge is-pointing m-down">Some help text</div>
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Positive</h2>
        <div class="js-markup" brush="xml">
            <div class="mf is-positive e-field">
                <label class="s-label">Positive Input</label>
                <div class="mf s-control">
                    <div class="mf e-badge is-pointing m-down">Some help text</div>
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Loading</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field is-loading">
                <label class="s-label">Loading e-field</label>
                <input class="s-input" type="text"  placeholder="Text Input" />
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Inline</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-fields">
                <div class="mf e-field u-inline">
                    <label class="s-label u-inline">Inline Inputs</label>
                    <div class="mf s-control u-inline">
                        <input class="s-input" type="text"  placeholder="Text Input" />
                    </div>
                </div>
                <div class="mf e-field u-inline">
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Badge</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field">
                <label class="s-label">Text Input + Badge</label>
                <div class="mf s-control">
                    <div class="mf e-badge is-pointing m-down">Some help text</div>
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Icon</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field">
                <label class="s-label">Icon Label - Prepend</label>
                <div class="mf s-control has-icon m-prepend">
                    <input class="s-input" type="text"  placeholder="Text Input" />
                    <i class="e-icon fa fa-search"></i>
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Action</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-field">
                <label class="s-label">Action - Append</label>
                <div class="mf s-control has-action m-append">
                    <input class="s-input" type="text"  placeholder="Text Input" />
                    <a href="" title="" class="mf e-button">Button</a>
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Checkbox</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-fields">
                <div class="mf u-inline e-checkbox">
                    <input class="s-input" type="checkbox" />
                    <label class="s-label">Checkbox</label>
                </div>
                <div class="mf u-inline e-checkbox">
                    <input disabled class="s-input" type="checkbox" />
                    <label class="s-label">Disabled</label>
                </div>
                <div class="mf u-inline e-checkbox">
                    <input disabled checked class="s-input" type="checkbox" />
                    <label class="s-label">Disabled Checked</label>
                </div>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Radio</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-fields">
                <div class="mf e-radio u-inline">
                    <input checked class="s-input" type="radio" name="radio" />
                    <label class="s-label">Radio Group</label>
                </div>
                <div class="mf e-radio u-inline">
                    <input class="s-input" type="radio" name="radio" />
                    <label class="s-label">Radio Group</label>
                </div>
                <div class="mf e-radio u-inline">
                    <input disabled class="s-input" type="radio" name="radio" />
                    <label class="s-label">Disabled</label>
                </div>
            </div>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
