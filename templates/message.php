<?php include('partials/header.php') ?>

    <!-- START .main -->

    <div class="l-container">
        <h1 class="h1">Message</h1>
        <hr/>
        <?php fileInfo(array('location' => 'element/message.less')); ?>
    </div>

    <div class="l-container">
        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-message">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing massa.</li>
                    <li>Cum sociis natoque penatibus et magnis dis sem.</li>
                    <li>Nulla consequat massa quis enim. Donec pede justo</li>
                    <li>In enim justo, rhoncus ut, imperdiet a, venenatis pretium. Integer tincidunt.</li>
                </ul>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Positive</h2>
        <div class="js-markup" brush="xml">
            <div class="mf m-positive e-message">
                <p>Positive Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing massa.</li>
                    <li>Cum sociis natoque penatibus et magnis dis sem.</li>
                </ul>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Negative</h2>
        <div class="js-markup" brush="xml">
            <div class="mf m-negative e-message">
                <p>Negative Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing massa.</li>
                    <li>Cum sociis natoque penatibus et magnis dis sem.</li>
                </ul>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Alert</h2>
        <div class="js-markup" brush="xml">
            <div class="mf m-alert e-message">
                <p>Alert Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing massa.</li>
                    <li>Cum sociis natoque penatibus et magnis dis sem.</li>
                </ul>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Info</h2>
        <div class="js-markup" brush="xml">
            <div class="mf m-info e-message">
                <p>Info Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing massa.</li>
                    <li>Cum sociis natoque penatibus et magnis dis sem.</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END .main -->

<?php include('partials/footer.php') ?>
