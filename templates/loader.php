<?php include('partials/header.php') ?>

    <!-- START .main -->

    <div class="l-container">
        <h1 class="h1">Loader</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/loader.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-loader"></div>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
