<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Badge</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/badge.less')); ?>

        <div class="l-grid">
            <div class="l-column">
                <h2 class="e-preview-title js-preview">Default</h2>
                <div class="js-markup" brush="xml">
                    <div class="mf e-badge">
                        Badge
                    </div>
                    <div class="mf m-positive e-badge">
                        Positive
                    </div>
                    <div class="mf m-negative e-badge">
                        Negative
                    </div>
                    <div class="mf m-alert e-badge">
                        Alert
                    </div>
                    <div class="mf m-info e-badge">
                        Info
                    </div>
                    <div class="mf m-info e-badge m-circular">
                        1
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END .main -->

<?php include('partials/footer.php') ?>
