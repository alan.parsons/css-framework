<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Form</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/form.less')); ?>

        <h2 class="e-preview-title js-preview">Form</h2>
        <div class="js-markup" brush="xml">
            <form class="mf e-form">

                <div class="mf e-field">
                    <label class="s-label">Input</label>
                    <input class="s-input" type="text"  placeholder="Text Input" />
                </div>
                <div class="mf e-field">
                    <label class="s-label">Select Text</label>
                    <select>
                        <option>Select Text</option>
                    </select>
                </div>
                <div class="mf e-field">
                    <label class="s-label">Textarea</label>
                    <textarea class="s-input"></textarea>
                </div>
                <div class="e-actions">
                    <input type="button" class="mf e-button" value="Send Enquiry" />
                </div>
            </form>
        </div>

        <h2 class="e-preview-title js-preview">Legend</h2>
        <div class="js-markup" brush="xml">
            <fieldset>
                <legend>Legend:</legend>
                <div class="mf e-field">
                    <label class="s-label">Text Input + Badge</label>
                    <div class="mf s-control">
                        <div class="mf e-badge is-pointing m-down">Some help text</div>
                        <input class="s-input" type="text"  placeholder="Text Input" />
                    </div>
                </div>
            </fieldset>
        </div>

        <h2 class="e-preview-title js-preview">Loading</h2>
        <div class="js-markup" brush="xml">
            <form class="mf e-form is-loading">
                <fieldset>
                    <legend>Legend:</legend>
                    <div class="mf e-field">
                        <label class="label">Text Input</label>
                        <input class="input" type="text"  placeholder="Text Input" />
                    </div>
                    <div class="e-actions">
                        <input type="button" class="mf e-button" value="Send Enquiry" />
                    </div>
                </fieldset>
            </form>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
