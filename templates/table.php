<?php include('partials/header.php') ?>

    <!-- START .main -->

    <div class="l-container">
        <h1 class="h1">Table</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/table.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <table class="mf m-striped e-table">
                <thead>
                    <tr class="row">
                        <th class="s-cell m-head">Entry Header 1</th>
                        <th class="s-cell m-head">Entry Header 2</th>
                        <th class="s-cell m-head">Entry Header 3</th>
                        <th class="s-cell m-head">Entry Header 4</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="row">
                        <td class="s-cell">Entry First Line 1</td>
                        <td class="s-cell">Entry First Line 2</td>
                        <td class="s-cell">Entry First Line 3</td>
                        <td class="s-cell">Entry First Line 4</td>
                    </tr>
                    <tr class="row">
                        <td class="s-cell">Entry Line 1</td>
                        <td class="s-cell">Entry Line 2</td>
                        <td class="s-cell">Entry Line 3</td>
                        <td class="s-cell">Entry Line 4</td>
                    </tr>
                    <tr class="row">
                        <td class="s-cell">Entry Last Line 1</td>
                        <td class="s-cell">Entry Last Line 2</td>
                        <td class="s-cell">Entry Last Line 3</td>
                        <td class="s-cell">Entry Last Line 4</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
