<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">

        <h1 class="h1">Headings</h1>
        <hr/>
        <h1 class="h1">Heading 1</h1>
        <h2 class="h2">Heading 2</h2>
        <h3 class="h3">Heading 3</h3>
        <h4 class="h4">Heading 4</h4>
        <h5 class="h5">Heading 5</h5>

        <h1 class="h1">Paragraphs</h1>
        <hr/>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>
        <p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.</p>


    </div>

    <div class="l-container">
        <h1 class="h1">Lists</h1>
        <hr/>
        <ul>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean
            massa.</li>
            <li>Cum sociis natoque penatibus et magnis dis
            parturient montes, nascetur ridiculus mus. Donec quam
            felis, ultricies nec, pellentesque eu, pretium quis,
            sem.</li>
            <li>Nulla consequat massa quis enim. Donec pede justo,
            fringilla vel, aliquet nec, vulputate eget, arcu.
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit.</li>
                    <li>Cum sociis natoque penatibus et magnis dis
                    parturient montes, nascetur ridiculus mus.</li>
                </ul>
            </li>
            <li>In enim justo, rhoncus ut, imperdiet a, venenatis
            vitae, justo. Nullam dictum felis eu pede mollis
            pretium. Integer tincidunt.</li>
        </ul>
        <ol>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing
            elit. Aenean commodo ligula eget dolor. Aenean
            massa.</li>
            <li>Cum sociis natoque penatibus et magnis dis
            parturient montes, nascetur ridiculus mus. Donec quam
            felis, ultricies nec, pellentesque eu, pretium quis,
            sem.</li>
            <li>Nulla consequat massa quis enim. Donec pede justo,
            fringilla vel, aliquet nec, vulputate eget, arcu.
                <ul>
                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing
                    elit.</li>
                    <li>Cum sociis natoque penatibus et magnis dis
                    parturient montes, nascetur ridiculus mus.</li>
                </ul>
            </li>
            <li>In enim justo, rhoncus ut, imperdiet a, venenatis
            vitae, justo. Nullam dictum felis eu pede mollis
            pretium. Integer tincidunt.</li>
        </ol>
    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
