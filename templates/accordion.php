<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">Accordion</h1>
        <hr/>

        <?php fileInfo(array('location' => 'collection/accordion.less')); ?>

        <div class="l-grid">
            <div class="l-column">
                <p>Example of accordions in CSS only, here I use the pseudo selector ":target" and no javascript for modal action.<br/>
                This works in IE9+ and all modern browsers. IE8 with selectivizr. Limitations is no open element on page load - Using jquery to enhance this.</p>
                <h2 class="e-preview-title js-preview">Tabs</h2>
                <div class="js-markup" brush="xml">
                    <div class="js-accordion e-accordion">
                        <a href="#tab-1" class="s-title m-top m-attached e-section is-current">Tab 1</a>
                        <span class="mf s-target is-current" id="tab-1"></span>
                        <div class="mf m-attached e-section s-content">Tab 1 content</div>
                        <a href="#tab-2" class="s-title m-attached e-section">Tab 2</a>
                        <span class="mf s-target" id="tab-2"></span>
                        <div class="mf m-bottom m-attached e-section s-content">Tab 2 content</div>
                    </div>
                </div>
                <h3>Settings</h3>
                <table class="mf m-striped e-table">
                    <thead>
                        <tr class="row">
                            <th class="s-cell m-head"></th>
                            <th class="s-cell m-head">Type</th>
                            <th class="s-cell m-head">Default</th>
                            <th class="s-cell m-head">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row">
                            <td class="s-cell">link</td>
                            <td class="s-cell">string</td>
                            <td class="s-cell">.s-title</td>
                            <td class="s-cell">The element that will be clicked to toggle between tabs</td>
                        </tr>
                        <tr class="row">
                            <td class="s-cell">history</td>
                            <td class="s-cell">bool</td>
                            <td class="s-cell">true</td>
                            <td class="s-cell">Toggles the histroy options using the hash attr</td>
                        </tr>
                        <tr class="row">
                            <td class="s-cell">target</td>
                            <td class="s-cell">string</td>
                            <td class="s-cell">.s-target</td>
                            <td class="s-cell">The anchor that will be targeted with the class is-current</td>
                        </tr>
                    </tbody>
                </table>
                <h3>Usage</h3>
                <p>The target is referenced via the href of the a tag</p>
                <p>There are two ways to call the settings via the data attr or via jQuery.</p>
                <p>Data attr is called using as follows <code>data-[MODULE]-[SETTING]</code> as below:</p>
                <div class="e-highlight m-example">
                    <pre class="brush: xml">
                        <div class="js-accordion" data-tabs-history="false">...</div>
                    </pre>
                </div>
                <p>jQuery as below:</p>
                <div class="e-highlight m-example">
                    <pre class="brush: js">
                        $('.js-accordion').tabs({
                            history: false
                        });
                    </pre>
                </div>
                <p>If you are calling the plugin on multiple elements you will need to wrap the call in a each function</p>
            </div>
        </div>
    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
