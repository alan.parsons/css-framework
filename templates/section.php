<?php include('partials/header.php') ?>

    <!-- START .main -->
    <div class="l-container">
        <h1 class="h1">e-section</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/section.less')); ?>

        <h2 class="e-preview-title js-preview">Default</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-section">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Colours</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-section m-blue">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-red">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-orange">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-yellow">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-purple">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-inverted">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Fill</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-section m-blue m-fill">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
        </div>
        <h2 class="e-preview-title js-preview">Attached</h2>
        <div class="js-markup" brush="xml">
            <div class="mf e-section m-attached m-top">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-section m-attached">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
            <div class="mf e-message m-info m-attached m-bottom">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes</p>
            </div>
        </div>
    </div>
    <!-- END .main -->

<?php include('partials/footer.php') ?>
