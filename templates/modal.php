<?php include('partials/header.php') ?>

    <div class="l-container">
        <h1 class="h1">Modal</h1>
        <hr/>

        <?php fileInfo(array('location' => 'element/modal.less')); ?>

        <div class="l-grid">
            <div class="l-column">
                <a href="#modal-one" title="" class="mf e-button m-blue js-modal">Modal</a>
                <p>Example of modal in CSS only, here I use the pseudo selector ":target" and no javascript for modal action.<br/>
                This works in IE9+ and all modern browsers. IE8 with selectivizr.</p>
                <h3>Settings</h3>
                <table class="mf m-striped e-table">
                    <thead>
                        <tr class="row">
                            <th class="s-cell m-head"></th>
                            <th class="s-cell m-head">Type</th>
                            <th class="s-cell m-head">Default</th>
                            <th class="s-cell m-head">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="row">
                            <td class="s-cell">history</td>
                            <td class="s-cell">bool</td>
                            <td class="s-cell">true</td>
                            <td class="s-cell">Toggles the histroy options using the hash attr</td>
                        </tr>
                        <tr class="row">
                            <td class="s-cell">close</td>
                            <td class="s-cell">string</td>
                            <td class="s-cell">[href="#close"]</td>
                            <td class="s-cell">The targeted element to close the sidebar</td>
                        </tr>
                    </tbody>
                </table>
                <h3>Usage</h3>
                <p>The target is referenced via the href of the a tag</p>
                <p>There are two ways to call the settings via the data attr or via jQuery.</p>
                <p>Data attr is called using as follows <code>data-[MODULE]-[SETTING]</code> as below:</p>
                <div class="e-highlight m-example">
                    <pre class="brush: xml">
                        <a href="#modal-one" title="" data-modal-history="false" class="mf e-button m-blue">Modal</a>
                    </pre>
                </div>
                <p>jQuery as below:</p>
                <div class="e-highlight m-example">
                    <pre class="brush: js">
                        $('js-modal').modal({
                            history: false
                        });
                    </pre>
                </div>
                <p>If you are calling the plugin on multiple elements you will need to wrap the call in a each function</p>
            </div>
        </div>

        <h2 class="e-preview-title js-preview">Modal</h2>
        <div class="js-markup" brush="xml">
            <!-- Modal -->
            <div class="mf e-modal" id="modal-one" aria-hidden="true">
                <a href="#close" class="s-overlay" aria-hidden="true"></a>
                <div class="s-content">
                    <div class="s-header">
                        <a href="#close" class="s-close" aria-hidden="true">×</a> <!--CHANGED TO "#close"-->
                        Modal in CSS?
                    </div>
                    <div class="s-body">
                        <p>One modal example here! :D</p>
                    </div>
                    <div class="s-footer">
                        <a href="#close" title="Close" class="mf e-button m-positive">Nice!</a>
                    </div>
                </div>
            </div>
            <!-- /Modal -->
        </div>

    </div>

    <!-- END .main -->

<?php include('partials/footer.php') ?>
